################################################################################
# Dockerfile de construção do container Solr utilizado pelo SEI e pelo SIP
#
# Container preparado e configurado para uso em desenvolvimento e testes
################################################################################

#TODO: Migrar para container oficial do Java 8 (8-jdk-alpine)
FROM centos:centos7
MAINTAINER Welton Rodrigo Torres Nascimento <welton.torres@prf.gov.br>

############################# INÍCIO DA INSTALAÇÃO #############################
ENV TERM xterm

COPY assets/msttcore-fonts-2.0-3.noarch.rpm /tmp
COPY assets/index/* /tmp/
COPY assets/sei-solr-6.1.0.sh /tmp/sei-solr-6.1.0.sh
COPY install.sh /install.sh

RUN sh /install.sh
############################## FIM DA INSTALACAO ###############################

EXPOSE 8983

# ENTRYPOINT permite que o docker daemon passe signais para a aplicação, como o SIGTERM,
# isso permite que a aplicação consiga se encerrar sozinha quando executamos
# docker stop.
ENTRYPOINT ["/opt/solr/bin/solr", "start", "-f", "-p", "8983"]
