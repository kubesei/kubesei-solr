# Container do Solr para SEI 3.0

Este container deverá ser usado para o deploy do Sei no kubernetes.

Foi utilizado como ponto de partida a excelente versão feita pelo Guilheme
Cantoni e colaboradores ( https://softwarepublico.gov.br/gitlab/sei/sei-vagrant
)

Este é um fork, mas só da subtree solr.
